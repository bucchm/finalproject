﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour {

    public GameObject player;
    //public GameObject character;
    public Rigidbody rb;
    public Vector3 currentPosition;
    public Quaternion currentRotation;
    Vector3 moveDir;
    int speed = 180;

    public GameObject Heart3;
    public GameObject Heart2;
    public GameObject Heart1;

    private float coolDown = 3;
    private float coolDownTimer;

    private float VertRotate = 0;
    private float HoriRotate = 0;

    public GameObject DamageParticle;

    // Use this for initialization
    void Start () {
        Heart3.SetActive(true);
        Heart2.SetActive(false);
        Heart1.SetActive(false);

        rb = GetComponent<Rigidbody>();
       currentPosition = transform.position;

        DamageParticle.SetActive(false);
    }

    void Update()
    {
        DamageParticle.transform.position = currentPosition + new Vector3(0, -0.010f, -1);
        //Cooldown for when hit / invulnerability period
        if (coolDownTimer > 0)
        {
            coolDownTimer -= Time.deltaTime;
            DamageParticle.SetActive(true);

        }
        if (coolDownTimer < 0)
        {
            coolDownTimer = 0;
        }
        if (coolDownTimer == 0)
        {
            DamageParticle.SetActive(false);
        }

        Debug.Log("Cooldown is " + coolDownTimer);

    }

    // Update is called once per frame
    void FixedUpdate() {
        //Movement and character rotation
        moveDir = Vector3.zero;
        moveDir.x = Input.GetAxis("Horizontal");
        moveDir.y = Input.GetAxis("Vertical");
        rb.velocity = new Vector3(moveDir.x * speed * Time.deltaTime, moveDir.y * speed * Time.deltaTime, 0);
        currentPosition = transform.position;

        if (Input.GetKey("space"))
        {
            speed = 300;
            Debug.Log("Space is pressed");
        }
        else
        {
            speed = 180;
        }

        HoriRotate = -30 * moveDir.x;
        VertRotate = -15 * moveDir.y;

        transform.rotation = Quaternion.Euler(VertRotate, HoriRotate, 0);
    }

    void OnCollisionEnter(Collision collision)
    {
    //Collision with wall, avoid being pushed by attacks
        if (collision.gameObject.tag != "Impact")
        {
            rb.velocity = new Vector3(0, 0, 0);
            Debug.Log("Touch2");
        }
        
    }

    void OnTriggerEnter(Collider other)
    {
    //LifeSystem
        if (other.gameObject.tag == "Impact")
        {
            if (Heart3.activeSelf == true && coolDownTimer == 0)
            {
                Debug.Log("Touch");
                Heart3.SetActive(false);
                Heart2.SetActive(true);
                coolDownTimer = coolDown;
            }

            if (Heart2.activeSelf == true && coolDownTimer == 0)
            {
                Debug.Log("Touch");
                Heart2.SetActive(false);
                Heart1.SetActive(true);
                coolDownTimer = coolDown;
            }

            if (Heart1.activeSelf == true && coolDownTimer == 0)
            {
                Debug.Log("Touch");
                Heart1.SetActive(false);
                coolDownTimer = coolDown;
            }

            if (Heart3.activeSelf == false && Heart2.activeSelf == false && Heart1.activeSelf == false && coolDownTimer == 0)
            {
                DamageParticle.SetActive(true);
                player.SetActive(false);
            }

        }
    }
}
