﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAnimation : MonoBehaviour {

    private Animator myAnimator;
    private static bool testCheck;

    private float coolDown = 2;
    private float coolDownTimer;

    public GameObject Music;
    public GameObject DefaultLights;
    private Vector3 InitialLightPosition;
    public GameObject PlayerLight;
    public GameObject DarkLights;

    private float LightIntensity = 1;
    private float MaxDistance = 200;
    private float Movement;

    // Use this for initialization
    void Start () {
		myAnimator = GetComponent<Animator>();
        Music.SetActive(false);
        DefaultLights.SetActive(true);
        InitialLightPosition = DefaultLights.transform.position;
        (PlayerLight.GetComponent("Halo") as Behaviour).enabled = false;
    }

    // Update is called once per frame
    void Update() {

        Debug.Log("Timer =" + coolDownTimer);
        testCheck = ShutterMovement.test;

        if (testCheck == true)
        {
            if (coolDownTimer > 0)
            {
                coolDownTimer -= Time.deltaTime;

            }
            if (coolDownTimer < 0)
            {
                coolDownTimer = 0;
            }
            if (coolDownTimer == 0)
            {
                myAnimator.SetBool("isMoving", true);
                Music.SetActive(true);
            }
        }
        if (testCheck == false)
        {
            coolDownTimer = coolDown;
        }

        if (myAnimator.GetCurrentAnimatorStateInfo(0).IsName("Darkness"))
        {
            if (DefaultLights.transform.position.z < MaxDistance && DefaultLights.transform.position.z > -1)
            {
                Movement = Time.deltaTime * 100;
                DefaultLights.transform.position += new Vector3(0, 0, Movement);
            } else
            {
                Movement = 0;
            }

            if (LightIntensity > 0)
            {
                LightIntensity -= Time.deltaTime;

            }
            if (LightIntensity < 0)
            {
                LightIntensity = 0;
            }
            RenderSettings.ambientIntensity = LightIntensity;
            (PlayerLight.GetComponent("Halo") as Behaviour).enabled = true;
        }

        if (myAnimator.GetCurrentAnimatorStateInfo(0).IsName("WhoAreYou"))
        {
            if (LightIntensity > -1)
            {
                LightIntensity += Time.deltaTime/4;

            }
            if (LightIntensity > 0.2f)
            {
                LightIntensity = 0.2f;
            }
            RenderSettings.ambientIntensity = LightIntensity;
        }

        if (myAnimator.GetCurrentAnimatorStateInfo(0).IsName("Brightness"))
        {
            if (LightIntensity > 6)
            {
                LightIntensity = 6;

            }
            if (LightIntensity > -1)
            {
                LightIntensity += Time.deltaTime;
            }
            RenderSettings.ambientIntensity = LightIntensity;
        }

        if (myAnimator.GetCurrentAnimatorStateInfo(0).IsName("Calming"))
        {
            DefaultLights.transform.position = InitialLightPosition + new Vector3(0, -70, 0);
            (PlayerLight.GetComponent("Halo") as Behaviour).enabled = false;
            if (LightIntensity > 1)
            {
                LightIntensity -= Time.deltaTime;

            }
            if (LightIntensity < 1)
            {
                LightIntensity = 1;
            }
            RenderSettings.ambientIntensity = LightIntensity;
        }

            if (myAnimator.GetCurrentAnimatorStateInfo(0).IsName("Ending"))
        {
            
            if (DefaultLights.transform.position.y < 9 && DefaultLights.transform.position.y > -80)
            {
                Movement = Time.deltaTime * 30;
                DefaultLights.transform.position += new Vector3(0, Movement, 0);
            }
            else
            {
                Movement = 0;
                DefaultLights.transform.position = InitialLightPosition;
            }
            RenderSettings.ambientIntensity = 1;
        }

        //Debug.Log("Light Intense = " + LightIntensity);
        Debug.Log(Music.activeSelf);
    }
}
