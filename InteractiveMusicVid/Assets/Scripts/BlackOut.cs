﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BlackOut : MonoBehaviour {

    private Animator myAnimator;
    public GameObject player;

    // Use this for initialization
    void Start () {
        myAnimator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        if (player.activeSelf == false)
        {
            myAnimator.SetBool("end", true);
        }

        if (myAnimator.GetCurrentAnimatorStateInfo(0).IsName("Restart"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

    }
}
