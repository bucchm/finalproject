﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {
    Transform thisTrans;
    public Vector3 currentRotation;
    public float correctRotation = 0.0f;
    public float smoothness = 5.0f;

    [SerializeField]
    Transform player;

    // Use this for initialization
    void Start () {
        thisTrans = this.transform;
    }
	
	// Update is called once per frame
	void Update () {
            thisTrans.position = player.position - new Vector3(player.position.x * 0.9f, player.position.y * 0.9f, 0);
    }
}